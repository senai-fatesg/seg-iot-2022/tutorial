# Tutorial

## Configuração da placa

![alt text](imagens/arduino_config_0.png "Acesse este Menu")

![alt text](imagens/arduino_config_1.png "Configure com o link")

```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
```

Depois, baixe a placa ESP 32

![alt text](imagens/arduino_config_2.png "Configure com o link")

![alt text](imagens/arduino_config_3.png "Configure com o link")

Você também vai precisar do driver para conectar o ESP32 no computador

https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers

## Configuração da biblioteca DHT 11

Testamos com a DHT 1.3.8

![alt text](imagens/arduino_config_4.png "Configure com o link")

## Configuração da biblioteca PubSubClient


![alt text](imagens/arduino_config_5.png "Configure com o link")

## Configuração do WiFi

Baixe o arquivo: [WiFi.zip](files/WiFi-1.2.7.zip)

![alt text](imagens/arduino_config_6.png "Configure com o link")